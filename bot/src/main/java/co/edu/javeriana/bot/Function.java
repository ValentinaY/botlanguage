package co.edu.javeriana.bot;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

public class Function {

	private String name;
	private List<String> inputParameters;
	private Viscera viscera;
	
	public Object execute(Stack<Map<String, Object>>  symbolTables, ProgrammData programmData) {
		programmData.addFunction(this);
		return null;
	}

	public Object executeFunction(List<ASTNode> parameters,Stack<Map<String,Object>>  symbolTables,ProgrammData programmData){
		Map<String,Object> symbolTable = new HashMap<String, Object>();
		for (int i=0;i<inputParameters.size();i++){
			symbolTable.put(this.inputParameters.get(i), parameters.get(i).execute(symbolTables, programmData));
		}
		symbolTables.push(symbolTable);
		Object return_=this.viscera.execute(symbolTables, programmData);
		symbolTables.pop();
		if(return_ !=null && return_  instanceof Return){
			return ((Return) return_ ).getReturn_();
		}
		return null;
	}

	public String getFunctionName() {
		return name;
	}
}
